import React from 'react'
import ReactDOM from 'react-dom'
import RegistrationApp from './components/RegistrationApp'
import 'normalize.css/normalize.css'
import './styles/styles.scss'

ReactDOM.render(<RegistrationApp />, document.getElementById('app'))