import React from 'react';
import {Container, Col, Row } from 'reactstrap';

import LogosThankPage from "./LogosThankPage";
import '../styles/components/ThankYouPage.css';
import './HomePage'

class ThankYouPage extends React.Component{
  onSubmit = () =>{
    //e.preventDefault()
    //this.create()
    //alert('inside clickHomepage');
    this.props.history.push('/')
  }
  render(){
    return (
      <div className="background">
        <Container>
          <LogosThankPage/>

          <Row>
            <Col md = { 12 }>
              <div>
                <h1 className="titulo-grcs">¡ MUCHAS GRACIAS !</h1>
                <p className="sub-titulo"> Tu mensaje fue enviado con éxito,
                  en breve se le notificará a la persona que visitas y te atenderá.</p>
              </div>

              <button onClick={this.onSubmit} type="submit" className="btn btn-danger btn-formu btn-form">
                Salir
              </button>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default ThankYouPage;
