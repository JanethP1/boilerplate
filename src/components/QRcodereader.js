import React, { Component } from 'react'
import QrReader from 'react-qr-reader'
import{withRouter} from 'react-router-dom'
export class QRcodereader extends Component

{
  constructor(props){
    super(props)
        this.state = {
      delay: 800,
      
      isHidden: true
    }
   
    this.handleScan = this.handleScan.bind(this)
  }
  handleScan(result){
    if(result){
      this.setState({ result })
    }
  }
  handleError(err){
    console.error(err)
  }

  toggleHidden(){
    this.setState({isHidden: !this.state.isHidden})
  }
  render(){
    const previewStyle = {
      height: 240,
      width: 320,
    }

     return(
      <div>
        <button onClick={this.toggleHidden.bind(this)}>
        QRcodereader!
         </button>
         {!this.state.isHidden && 
        <QrReader
        
          delay={this.state.delay}
          style={previewStyle}
          onError={this.handleError}
          onScan={this.handleScan}
          
          />}
          <br></br><br></br><br></br><br></br>
          <p>{this.state.result}</p>
      </div>
    )
  }
}
export  default withRouter(QRcodereader) ;