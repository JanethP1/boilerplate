import React from 'react'
import{withRouter} from 'react-router-dom'
import Reflux from 'reflux';
import 'bootstrap/dist/css/bootstrap.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faEnvelope, faBuilding,faMobileAlt } from '@fortawesome/free-solid-svg-icons'

import {
    Button, Card, CardBody, CardFooter, Label,
    CardHeader,Container, Col, Form, FormGroup, FormControl, InputGroup, InputGroupAddon, InputGroupText, Input, Row,Alert
} from 'reactstrap'
//import VisitsActions from './../actions/VisitsActions'
//import UserstStore from './../stores/UsersStore'
import EmployeeOnlineActions from './../actions/EmployeeOnlineActions'
import EmployeeOnline from './../stores/EmployeeOnline'
import { Utils } from './../actions/Utils'
import Test from './Test'
//import Qrcode from './Qrcode'
//import QRcodereader from './QRcodereader'
import Logos from "./Logos";
import '../styles/components/Employee.css';

class Employee extends Reflux.Component{
    constructor(props) {
        super(props)
        this.stores =[EmployeeOnline]
        this.storeKeys = ['user','errors']
        this.init = this.init.bind(this)
        //this.setUpdate = this.setUpdate.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleSaveClick = this.handleSaveClick.bind(this)
        this.create = this.create.bind(this)
        //this.update = this.update.bind(this)
    }
    handleChange = (obj, property, e) => {
        Utils.setValue(obj, property, e.target.value)
        this.setState({ user: obj });
    }

    handleSaveClick = (e) =>{
      e.preventDefault()
        this.create()
       // alert('inside employee online create');
        console.log(this.state.user)
        this.props.history.push('/gracias')
        
        
    }


  
    create() {
        let { user } = this.state
        EmployeeOnlineActions.create(user)
       // alert('inside createmethod');
      // VisitsActions.create(user)
      }
    
      init() {
        this.setState({
          user: {id:0,visitorname:"", visitoremail:"",visitororgname:"",visitormnumber:"",meetingstartdate:"",meetingenddate :"",orgpersonname:"",orgpersonemail:"",department:""},
          //user: {id:0,visitorname:"", visitoremail:"",visitororgname:"",visitormnumber:"",meetingstartdate:"",meetingenddate :"",orgpersonname:"",orgpersonemail:"",department:"",qrcode :""},
          errors: { val: undefined, message: undefined }
        })
      }
 
      
    render(){
        return ( 
            <div className="div-background">
                  <Logos/>
                  <Form >
                    <Row >
                      <Col>
                          <p className="subTitulo">Si no tiene cita, por favor llene el siguiente formulario.</p>
                            <FormGroup>
                              <InputGroup size="lg" >
                                <InputGroupAddon addonType="prepend">
                                <InputGroupText> <FontAwesomeIcon icon={faUser} /></InputGroupText>
                                </InputGroupAddon>
                                <Input  type="text" id="visitorname" placeholder="Nombre de usuario" value={this.state.user.visitorname} onChange={this.handleChange.bind(this, this.state.user, "visitorname")} required />
                              </InputGroup>
                            </FormGroup>

                              <FormGroup>
                                <InputGroup size="lg">
                                <InputGroupAddon addonType="prepend">
                                <InputGroupText> <FontAwesomeIcon icon={faEnvelope} /></InputGroupText>
                                </InputGroupAddon>
                                <Input   type="text" id="visitoremail" placeholder="correo electrónico" value={this.state.user.visitoremail} onChange={this.handleChange.bind(this, this.state.user, "visitoremail")} required />
                                </InputGroup>
                              </FormGroup>  

                              <FormGroup>
                                <InputGroup size="lg">
                                <InputGroupAddon addonType="prepend">
                                <InputGroupText> <FontAwesomeIcon icon={faBuilding} /></InputGroupText>
                                </InputGroupAddon>
                                <Input   type="text" id="visitororgname" placeholder="organización" value={this.state.user.visitororgname} onChange={this.handleChange.bind(this, this.state.user, "visitororgname")} required />
                                </InputGroup>
                              </FormGroup>
                              
                              <FormGroup>
                                <InputGroup size="lg">
                                <InputGroupAddon addonType="prepend">
                                <InputGroupText> <FontAwesomeIcon icon={faMobileAlt} /></InputGroupText>
                                </InputGroupAddon>
                                <Input  type="text" id="visitormnumber" placeholder="Telefono" value={this.state.user.visitormnumber} onChange={this.handleChange.bind(this, this.state.user, "visitormnumber")} required />
                                </InputGroup>
                              </FormGroup>
                  
                              <FormGroup>
                                <InputGroup>
                                <Input type="datetime-local" id="meetingstartdate" placeholder="meetingstartdate" value={this.state.user.meetingstartdate} onChange={this.handleChange.bind(this, this.state.user, "meetingstartdate")} required />
                                </InputGroup>
                              </FormGroup>
                                
                              <FormGroup>
                                <InputGroup>
                                <Input type="datetime-local" id="meetingenddate" placeholder="meetingenddate" value={this.state.user.meetingenddate} onChange={this.handleChange.bind(this, this.state.user, "meetingenddate")} required />
                                </InputGroup>
                              </FormGroup>
              
                              <FormGroup>
                                <InputGroup size="lg">
                                <InputGroupAddon addonType="prepend">
                                <InputGroupText> <FontAwesomeIcon icon={faUser} /></InputGroupText>
                                </InputGroupAddon>
                                <Input  type="text" id="orgpersonname" placeholder="Nombre de la persona de la organización" value={this.state.user.orgpersonname} onChange={this.handleChange.bind(this, this.state.user, "orgpersonname")} required />
                                </InputGroup>
                              </FormGroup>
                
                              <FormGroup>
                                <InputGroup size="lg">
                                <InputGroupAddon addonType="prepend">
                                <InputGroupText> <FontAwesomeIcon icon={faEnvelope} /></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="orgpersonemail" placeholder="Correo electrónico de la organización" value={this.state.user.orgpersonemail} onChange={this.handleChange.bind(this, this.state.user, "orgpersonemail")} required />
                                </InputGroup>
                              </FormGroup>
              
                              <FormGroup>
                                <InputGroup size="lg">
                                <InputGroupAddon addonType="prepend">
                                <InputGroupText> <FontAwesomeIcon icon={faBuilding} /></InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="department" placeholder="Departamento" value={this.state.user.department} onChange={this.handleChange.bind(this, this.state.user, "department")} required />
                                </InputGroup>
                              </FormGroup>
             
                              <FormGroup>
                              <Button onClick={this.handleSaveClick} type="submit" className="btn btn-danger btn-form">Continuar</Button>
                                
                              </FormGroup>
              </Col>
              </Row>
            </Form>         
            </div>
            )
    }
    
}



export default withRouter(Employee) 