import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import {Container, Col, Row } from 'reactstrap'
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import ToggleDisplay from 'react-toggle-display';
import Logos from "./Logos";
import '../styles/components/OnlineImage.css';
import gracias from './ThankYouPage'
import VisitsActions from './../actions/VisitsActions'
import UserstStore from './../stores/UsersStore'
import { Utils } from './../actions/Utils'
import Reflux from 'reflux';
function dataURItoBlob(dataURI) {

  let byteString = atob(dataURI.split(',')[1]);
  // separate out the mime component
  let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  let ab = new ArrayBuffer(byteString.length);
  let ia = new Uint8Array(ab);
  for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }
  let blob = new Blob([ab], { type: mimeString });
  return blob;
}

function padWithZeroNumber(number, width) {
  number = number + '';
  return number.length >= width ?
      number :
      new Array(width - number.length + 1).join('0') + number;
}

function getFileExtention(blobType) {
    // by default the extention is .png
    let extention = IMAGE_TYPES.PNG;

    if (blobType === 'image/jpeg') {
        extention = IMAGE_TYPES.JPG;
    }
    return extention;
}

function getFileName(imageNumber, blobType) {
    const prefix = 'photo';
    const photoNumber = padWithZeroNumber(imageNumber, 4);
    const extention = getFileExtention(blobType);

    return `${prefix}-${photoNumber}.${extention}`;
}

function downloadImageFileFomBlob(blob, imageNumber) {
    window.URL = window.webkitURL || window.URL;

    let anchor = document.createElement('a');
    anchor.download = getFileName(imageNumber, blob.type);
    anchor.href = window.URL.createObjectURL(blob);
    let mouseEvent = document.createEvent('MouseEvents');
    mouseEvent.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
    anchor.dispatchEvent(mouseEvent);
}

function downloadImageFile(dataUri, imageNumber) {
    let blob = dataURItoBlob(dataUri);
    downloadImageFileFomBlob(blob, imageNumber);
}

function toggleHidden() {
    this.setState({ isHidden: !this.state.isHidden })
}


class OnlineImage extends Reflux.Component {
    constructor(props) {
        super(props);
        this.imageNumber = 0;
        this.state = { show: false };
        this.stores =[UserstStore]
        this.storeKeys = ['user','errors']
        //this.init = this.init.bind(this)
        //this.setUpdate = this.setUpdate.bind(this)
        //this.handleChange = this.handleChange.bind(this)
        this.handleSaveClick = this.handleSaveClick.bind(this)
        this.create = this.create.bind(this)

    }

    onTakePhoto(dataUri) {
     //downloadImageFile(dataUri, this.imageNumber);
        //this.imageNumber += 1;
        this.props.location.state.detail.image=dataUri
        console.log(this.props.location.detail)
               
    }

    handleClick() {
        this.setState({
            show: !this.state.show
        });
    }

    handleSaveClick = (e) =>{
     e.preventDefault()
    console.log(this.props.location.state.detail);
    this.create()
      this.props.history.push('/gracias')
    }

    create() {
      let { user } = this.state
      VisitsActions.create(user)
    }



    render() {
        return (
          <div className="App div-background">
            <Container>

              <Logos/>
              <Row>
                <Col md={8}>
                  <div className="orientacion">
                    <button className="btn btn-link btn-foto" onClick = { () => this.handleClick() }>
                      Tomar foto
                    </button>
                    <ToggleDisplay show = { this.state.show }>
                      <Camera onTakePhoto = {
                          (dataUri) => { this.onTakePhoto(dataUri); }
                      }
                      imageType = { IMAGE_TYPES.PNG }
                      imageCompression = {0.97}
                      sizeFactor = {1}/>
                    </ToggleDisplay >

                  </div>
                </Col>
                <Col md={4}>
                  <button onClick={this.handleSaveClick} type="submit" className="btn btn-danger btn-form pa">
                    Continuar<i className="fas fa-chevron-right"></i>
                  </button>
                </Col>
              </Row>
            </Container>
          </div>
        );
    }
}


export default withRouter(OnlineImage);
