import React from 'react'
import {BrowserRouter, Switch, Route, Link} from 'react-router-dom'
import HomePage from './HomePage'
import NotFound from './NotFound'
import ThankYouPage from './ThankYouPage'
import Qrcode from './Qrcode'
import HomeRegistrationFormPage from './HomeRegistrationFormPage'
import QRcodereader from './QRcodereader'
//import Employee from './Employee'
import EmployeeRegister from './EmployeeRegister'
import Test from './Test'
import OnlineImage from './OnlineImage'

const RegistrationApp = () =>{
    return (
        <BrowserRouter>
        <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/EmployeeRegister" component={EmployeeRegister} />
            <Route path="/gracias/" component={ThankYouPage} />
            <Route path="/HomeRegistrationFormPage/" component={HomeRegistrationFormPage}/>
            <Route path="/Qrcode/" component={Qrcode} />
            <Route path="/QRcodereader/" component={QRcodereader} />
            <Route path="/OnlineImage/" component={OnlineImage} />

            <Route path="/Test/" component={Test} />
        </Switch>
        </BrowserRouter>
    )
}

export default RegistrationApp
