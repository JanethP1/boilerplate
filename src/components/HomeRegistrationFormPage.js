import React from 'react'
import{withRouter} from 'react-router-dom'
import Reflux from 'reflux';
import 'bootstrap/dist/css/bootstrap.css';
import {
    Button, Card, CardBody, CardFooter, Label,
    CardHeader,Container, Col, Form, FormGroup,
    FormControl, InputGroup, InputGroupAddon, InputGroupText, Input, Row,Alert
} from 'reactstrap'
import VisitsActions from './../actions/VisitsActions'
import UserstStore from './../stores/UsersStore'
import { Utils } from './../actions/Utils'
import OnlineImage from './OnlineImage'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser,faEnvelope,faMobileAlt, faBuilding } from '@fortawesome/free-solid-svg-icons'
import Logos from "./Logos";
import '../styles/components/HomeRegistrationFormPage.css';

class HomeRegistrationFormPage extends Reflux.Component{
    constructor(props) {
        super(props)
        this.stores =[UserstStore]
        this.storeKeys = ['user','errors']
        this.init = this.init.bind(this)
        //this.setUpdate = this.setUpdate.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleSaveClick = this.handleSaveClick.bind(this)
        //this.create = this.create.bind(this)
        //this.update = this.update.bind(this)
        this.state ={
          user:{},errors:{}
        }
    }
     
    handleValidation(){
      let user = this.state.user;
      let errors = {};
      let formIsValid = true;

      //Name
      if(!user["visitorname"]){
         formIsValid = false;
         errors["visitorname"] = "el campo no puede estar vacío";
         
      }

        //Email
      if(!user["vistoremail"]){
         formIsValid = false;
         errors["vistoremail"] = "el campo no puede estar vacío";
      }

      if(typeof user["vistoremail"] !== "undefined"){
        let lastAtPos = user["vistoremail"].lastIndexOf('@');
        let lastDotPos = user["vistoremail"].lastIndexOf('.');

        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && user["vistoremail"].indexOf('@@') == -1 && lastDotPos > 2 && (user["vistoremail"].length - lastDotPos) > 2)) {
           formIsValid = false;
           errors["vistoremail"] = "El correo no es válido";
         }
    }  
  
    //Mobile number
      if(!user["visitormnumber"]){
        formIsValid = false;
        errors["visitormnumber"] = "el campo no puede estar vacío";
     }

    //Organization name
    if(!user["visitororgname"]){
      formIsValid = false;
      errors["visitororgname"] = "el campo no puede estar vacío";
   }


    //Person to Meet
    if(!user["persontomeet"]){
      formIsValid = false;
      errors["persontomeet"] = "el campo no puede estar vacío";
   }


     this.setState({errors: errors});
     return formIsValid;
 }


    handleChange = (obj, property, e) => {
        Utils.setValue(obj, property, e.target.value)
        //alert(e.target.value)
        this.setState({ user: obj });

            }
    
            handleSaveClick = (e) =>{
              e.preventDefault()
              //this.create()
              //alert('inside create'+this.getState());
              //this.props.history.push('/OnlineImage')
              if (this.handleValidation()){
              this.props.history.push({ pathname: '/OnlineImage', state: { detail: this.state.user} })
              
            } else{
      
            }
          }

    onChange(e) {
      this.setState({file:e.target.files[0]});
    }

    create() {
      let { user } = this.state
      VisitsActions.create(user)
    }

    init() {
      this.setState({
      user: {visitorname:"", vistoremail:"",visitormnumber:"",visitororgname:"",persontomeet:"",image:""},
      errors: { val: undefined, message: undefined }
      })
    }

    render(){
      let validations = this.state.errors.message == undefined ? "" : (<Alert color={this.state.errors.val ? "success" : "warning"}> {this.state.errors.message}</Alert>)
        return (
            <div className="div-background">
              <Container>
                <Logos/>
                
                <Form>
                {validations}
                  <Row>
                  
                    <Col md={12}>
                    
                      <p className="subTitulo">
                        Si no tiene cita, por favor llene el siguiente formulario.
                      </p>
                      

                      <FormGroup>
                        <InputGroup size="lg">
                          <InputGroupAddon addonType="prepend">
                          <InputGroupText> <FontAwesomeIcon icon={faUser} /></InputGroupText>
                          </InputGroupAddon>
                          <Input  type="text" id="visitorname" placeholder="Nombre completo" value={this.state.user.visitorname} onChange={this.handleChange.bind(this, this.state.user, "visitorname")} required />
                        </InputGroup>
                        {this.state.errors["visitorname"]}
                      </FormGroup>

                      <FormGroup>
                        <InputGroup size="lg">
                        <InputGroupAddon addonType="prepend">
                        <InputGroupText> <FontAwesomeIcon icon={faEnvelope} /></InputGroupText>
                        </InputGroupAddon>
                        <Input  type="text" id="vistoremail" placeholder="correo electrónico" value={this.state.user.vistoremail} onChange={this.handleChange.bind(this, this.state.user, "vistoremail")} required />
                        </InputGroup>
                        {this.state.errors["vistoremail"]}
                      </FormGroup>

                      <FormGroup>
                        <InputGroup size="lg">
                        <InputGroupAddon addonType="prepend">
                        <InputGroupText> <FontAwesomeIcon icon={faMobileAlt} /></InputGroupText>
                        </InputGroupAddon>
                        <Input  type="text" id="visitormnumber" placeholder="Telefono" value={this.state.user.visitormnumber} onChange={this.handleChange.bind(this, this.state.user, "visitormnumber")} required />
                        </InputGroup>
                        {this.state.errors["visitormnumber"]}
                      </FormGroup>

                      <FormGroup>
                      <InputGroup size="lg">
                        <InputGroupAddon addonType="prepend">
                        <InputGroupText> <FontAwesomeIcon icon={faBuilding} /></InputGroupText>
                        </InputGroupAddon>
                          <Input type="text" id="visitororgname" placeholder="Organización" value={this.state.user.visitororgname} onChange={this.handleChange.bind(this, this.state.user, "visitororgname")} required />
                        </InputGroup>
                        {this.state.errors["visitororgname"]}
                      </FormGroup>

                      <FormGroup>
                      <InputGroup size="lg">
                        <InputGroupAddon addonType="prepend">
                        <InputGroupText> <FontAwesomeIcon icon={faUser} /></InputGroupText>
                        </InputGroupAddon>
                          <Input type="text" id="persontomeet" placeholder="Persona a quién visita" value={this.state.user.persontomeet} onChange={this.handleChange.bind(this, this.state.user, "persontomeet")} required />
                        </InputGroup>
                        {this.state.errors["persontomeet"]}
                      </FormGroup>

                

                      <Button onClick={this.handleSaveClick} type="submit" className="btn btn-danger btn-form">Continuar</Button>
                    </Col>
                  </Row>
                </Form>
              </Container>
            </div>
        );
    }
}



export default withRouter(HomeRegistrationFormPage);
