import React from 'react'
import {Container, Col, Row } from 'reactstrap'
import Qrcode from "./Qrcode"
//import QRcodereader from './QRcodereader'
import OnlineImage from './OnlineImage'
//import Employee from './Employee'
import HomeRegistrationFormPage from './HomeRegistrationFormPage'
import Logos from "./Logos";
import '../styles/components/HomePage.css';

class HomePage extends React.Component{
  onSubmit = () =>{
    //e.preventDefault()
    //this.create()
    //alert('inside clickHomepage');
    this.props.history.push('/HomeRegistrationFormPage')
  }
  render(){
    return (
      <div className="div-background">
        <Container>
          <Logos/>
          <Row>
            <Col md = { 12 }>
              <div className="titulo">
                <h1>¡Bienvenido!</h1>
                <p>Regístrate para ingresar</p>
                <button onClick={this.onSubmit} type="submit" className="btn btn-danger">
                  Continuar<i className="fas fa-chevron-right"></i>
                </button>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default HomePage;
