import React from 'react'
import {Col, Row } from 'reactstrap'
import logoFourthDimension from "../img/fourd.png";
import logoIsita from "../img/logo-isita.png";
import '../styles/BackgroundThankPage.css';

const LogosThankPage = () =>{
  return(
    <div>
      <Row>
        <Col md={6}>
          <img className="img-fourth-dimension" src={logoFourthDimension} />
        </Col>
        <Col md={6}>
          <img  className="img-isita" src={logoIsita} />
        </Col>
      </Row>
    </div>
  );
}
export default LogosThankPage;
