import React, { Component } from 'react'
import{withRouter} from 'react-router-dom'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import EmployeeOnlineActions from './../actions/EmployeeOnlineActions'
import EmployeeOnline from './../stores/EmployeeOnline'
export class Test extends Component{
  constructor (props) {
    super(props)
    this.state = {
     startDate: new Date()
    };
    console.log('helloooo'+this.state.startDate);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
   
  }


  handleChange(date) {
    console.log(date);
    //return date
    this.setState({
       startDate: date
      })
      
      //console.log('the date'+ date)
     // this.state.user.meetingstartdate = date
      //console.log('meeting date'+this.state.user.meetingstartdate)
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log('hellooo')
   // let main = this.state.startDate
  //  console.log(main.format('L'));
  }

  render() {
    return (
      <div >
        
        <form onSubmit={ this.handleSubmit }>
        <div className="form-group date-parent" >
          
           <DatePicker
           className="input-form"
              selected={ this.state.startDate }
              onChange={ this.handleChange }
              name="startDate"
              showTimeSelect
              timeFormat="HH:mm"
              timeIntervals={15}
              timeCaption="time"
              dateFormat="MM/dd/yyyy h:mm aa "
              
              //dateFormat="L"
            />
          </div>
          
        </form>
      </div>
    );
  
    return this;
  }
}
export default withRouter(Test) ;