import React, { Component } from 'react'
import{withRouter} from 'react-router-dom'
import QRCode from 'qrcode'

export class Qrcode extends Component {
    
     generateQR = (e) => {
          e.preventDefault()
         let str = 'My first QR!'
         QRCode.toCanvas(document.getElementById('canvas'), str, function(error) {
              if (error) console.error(error)
              console.log('success!')
         })
    }
    render() {
    return (
    <div >
         <canvas id="canvas" />
         <button className="btn btn-danger" onClick={this.generateQR}>
              Generate QR!
         </button>
    </div>
    )}
}
    export  default withRouter(Qrcode) ;