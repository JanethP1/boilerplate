import Reflux from 'reflux'
import axios from 'axios'
import {Utils} from './Utils'

var VisitsActions = Reflux.createActions({
    'loadVisits':{children: ['completed', 'failed']},
    'getvisit':{children: ['completed', 'failed']},
    'create':{children: ['completed', 'failed']},
    'update':{children: ['completed', 'failed']}
});

axios.defaults.baseURL = Utils.appConstants.BASE_URL
//axios.defaults.headers.common['Authorization'] = "
axios.defaults.headers.post['Content-Type'] = 'application/json';



VisitsActions.create.listen((user) => {
    let validation=validatevisit(user)
//alert('inside visit actions');
        delete user.id;
    if (validation.val){
       // alert('before post request');   
        console.log(user);    
        axios.post('user/register', user).then((response) => {
           // alert('after post request');
            VisitsActions.create.completed(response.data);
          }).catch((error) => {
            VisitsActions.create.failed(Utils.getErrors(error));
          });
    } else{
            VisitsActions.create.failed(validation);
    }
  });

   let validatevisit = (user) => {
    
    if (user.visitorname == "")
        return { message: Utils.fieldValidationMessage("Nombre completo"), val: false }

    if (user.vistoremail == "")
        return { message: Utils.fieldValidationMessage("correo electrónico"), val: false }

    if (user.visitormnumber == "")
        return { message: Utils.fieldValidationMessage("Telefono"), val: false }     
      
    if (user.visitororgname == "")
    return { message: Utils.fieldValidationMessage("Organización"), val: false }       
    
    if (user.persontomeet == "")
        return { message: Utils.fieldValidationMessage("Persona a quién visita"), val: false }
    if (user.image == "")
        return { message: Utils.fieldValidationMessage("Photo"), val: false }     
        
    return { val: true, message: "" }
};


export default VisitsActions;
