export const Utils = {

    appConstants: {
      BASE_URL: 'https://isitavisitormanagement.azurewebsites.net/api/',
      //token: 'Bearer' + ''
      //BASE_URL: 'https://residenciasapi.azurewebsites.net'
    },
  
    DAYS_WEEK: [
      {name:'Lunes',value:1},
      {name:'Martes',value: 2 },
      {name:'Miercoles',value:4},
      {name:'Jueves',value:8 },
      {name:'Viernes',value:16},
      {name:'Sabado',value: 32 },
      {name:'Domingo',value:64}
    ],
  
    getErrors: (error) => {
      console.log(error);
      console.log('HTTP ERROR', error.statusText);
      if (error.data === undefined)
        return 'Ha ocurrido un error'
      else {
        console.log('Api error: ', error.data.errors);
        return error.data.errors;
      }
    },
  
    setValue: (obj, key, value) => {
      var arr = key.split(".");
      var current = arr.shift();
      if (arr.length < 1) {
        obj[key] = value;
      } else if (typeof obj[current] === 'object') {
        key = arr.join('.')
        this.setValueFromObject(obj[current], key, value)
      }
    },
  
    getValue: (obj, key) => {
      var arr = key.split(".");
      var current = arr.shift();
      if (arr.length < 1) {
        return obj[key];
      } else if (typeof obj[current] === 'object') {
        key = arr.join('.')
        this.getValue(obj[current], key)
        return this.getValue(obj[current], key)
      }
    },
  
    fieldValidationMessage: (field) =>{
      return `El campo \"${field}\" es requerido`
    },
  
    validateNumber: (number) =>{
      var patt = new RegExp("^\\d+(?:\\.\\d+)?$")
      return  patt.test(number)
    }
  }
  