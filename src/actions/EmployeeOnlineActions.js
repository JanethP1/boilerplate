import Reflux from 'reflux'
import axios from 'axios'
import {Utils} from './Utils'

var EmployeeOnlineActions = Reflux.createActions({
    'loadVisits':{children: ['completed', 'failed']},
    'getvisit':{children: ['completed', 'failed']},
    'create':{children: ['completed', 'failed']},
    'update':{children: ['completed', 'failed']}
});

axios.defaults.baseURL = Utils.appConstants.BASE_URL
//axios.defaults.headers.common['Authorization'] = "
axios.defaults.headers.post['Content-Type'] = 'application/json';



EmployeeOnlineActions.create.listen((user) => {
    let validation=validatevisit(user)
//alert('inside employeeonline actions');
        delete user.id;
    if (validation.val){
       // alert('before post request');   
        console.log(user);    
        axios.post('user/onlineregister', user).then((response) => {
            //alert('after post request');
            EmployeeOnlineActions.create.completed(response.data);
          }).catch((error) => {
            EmployeeOnlineActions.create.failed(Utils.getErrors(error));
          });
    } else{
        EmployeeOnlineActions.create.failed(validation);
    }
  });

   let validatevisit = (user) => {
    
    if (user.visitorname == "")
        return { message: Utils.fieldValidationMessage("Nombre"), val: false }

    if (user.visitoremail == "")
        return { message: Utils.fieldValidationMessage("Correo"), val: false }

    if (user.visitororgname == "")
        return { message: Utils.fieldValidationMessage("organization"), val: false } 

     if (user.visitormnumber == "")
        return { message: Utils.fieldValidationMessage("Telefono"), val: false }    
      
    if (user.meetingstartdate == "")
    return { message: Utils.fieldValidationMessage("fecha de inicio"), val: false }  
    
    if (user.meetingenddate == "")
    return { message: Utils.fieldValidationMessage("fecha final"), val: false }

    if (user.orgpersonname == "")
    return { message: Utils.fieldValidationMessage("Nombre de la persona de la organización"), val: false }

    if (user.orgpersonemail == "")
    return { message: Utils.fieldValidationMessage("Correo electrónico de la organización"), val: false }

    if (user.department == "")
    return { message: Utils.fieldValidationMessage("Departamento"), val: false }
 
               
    return { val: true, message: "" }
};


export default EmployeeOnlineActions;
