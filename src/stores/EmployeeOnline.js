import Reflux from 'reflux'
import EmployeeOnlineActions from '../actions/EmployeeOnlineActions'

export default class EmployeeOnline extends Reflux.Store {

    constructor(){
        super();
        this.state = {
            users: [],
            user: {id:0,visitorname:"", visitoremail:"",visitororgname:"",visitormnumber:"",meetingstartdate:"",meetingenddate :"",orgpersonname:"",orgpersonemail:"",department:""},
            //user: {id:0,visitorname:"", visitoremail:"",visitororgname:"",visitormnumber:"",meetingstartdate:"",meetingenddate :"",orgpersonname:"",orgpersonemail:"",department:"",qrcode :""},
            errors: { val: undefined, message: undefined },
            is_update: false
        }
        this.listenables = EmployeeOnlineActions;
    }
    onLoadUsersCompleted(data){
        this.setState({
            users: data
        })
    }
    onLoadUsersFailed(error){
        console.log(error);
    }
    onGetUserCompleted(data){
        this.setState({
            user: data,
        })
    }
    onGetUserFailed(error){
        console.log(error);
    }
    
    onCreateCompleted(data) {

        //alert("PRUEBA")
        this.setState({
            user: {id:0,visitorname:"", visitoremail:"",visitororgname:"",visitormnumber:"",meetingstartdate:"",meetingenddate :"",orgpersonname:"",orgpersonemail:"",department:""},
            //user: {id:0,visitorname:"", visitoremail:"",visitororgname:"",visitormnumber:"",meetingstartdate:"",meetingenddate :"",orgpersonname:"",orgpersonemail:"",department:"",qrcode :""},
            errors: { message: data.message, val: true }
        })
    }

    onCreateFailed(errors) {
        this.setState({ errors: errors })
    }

    onUpdateCompleted(data) {
        this.setState({
            user: {id:0,visitorname:"", visitoremail:"",visitororgname:"",visitormnumber:"",meetingstartdate:"",meetingenddate :"",orgpersonname:"",orgpersonemail:"",department:""},
            //user: {id:0,visitorname:"", visitoremail:"",visitororgname:"",visitormnumber:"",meetingstartdate:"",meetingenddate :"",orgpersonname:"",orgpersonemail:"",department:"",qrcode :""},
            errors: { message: data.message, val: true }
        })
    }
    onUpdateFailed(errors){
        this.setState({errors:errors})
    } 
}
