import Reflux from 'reflux'
import VisitsActions from './../actions/VisitsActions'

export default class UsersStore extends Reflux.Store {

    constructor(){
        super();
        this.state = {
            users: [],
            user: {visitorname:"", vistoremail:"",visitormnumber:"",visitororgname:"",persontomeet:""},
            errors: { val: undefined, message: undefined },
            is_update: false
        }
        this.listenables = VisitsActions;
    }
    onLoadUsersCompleted(data){
        this.setState({
            users: data
        })
    }
    onLoadUsersFailed(error){
        console.log(error);
    }
    onGetUserCompleted(data){
        this.setState({
            user: data,
        })
    }
    onGetUserFailed(error){
        console.log(error);
    }
    
    onCreateCompleted(data) {

       // alert("PRUEBA")
        this.setState({
            user: {visitorname:"", vistoremail:"",visitormnumber:"",visitororgname:"",persontomeet:""},
            errors: { message: data.message, val: true }
        })
    }

    onCreateFailed(errors) {
        this.setState({ errors: errors })
    }

    onUpdateCompleted(data) {
        this.setState({
            user: {visitorname:"", vistoremail:"",visitormnumber:"",visitororgname:"",persontomeet:""},
            errors: { message: data.message, val: true }
        })
    }
    onUpdateFailed(errors){
        this.setState({errors:errors})
    } 
}
